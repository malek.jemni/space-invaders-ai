using System.Collections.Generic;

public class GameState
{
    public AStar.Node.NodeBase PlayerPosition { get; set; }
    public List<AStar.Node.NodeBase> EnemyPositions { get; set; }
    public bool IsPatrolling { get; set; }
 
}
