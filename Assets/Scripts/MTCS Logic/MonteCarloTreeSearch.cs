using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Zayene.UnityAITools.BehaviourTree;

public class MonteCarloTreeSearch
{
    private MCTSNode root;

    public MonteCarloTreeSearch(NodeState initialState)
    {
        root = new MCTSNode(initialState);
        
    }

    public NodeState RunMCTS(int iterations)
    {
        for (int i = 0; i < iterations; i++)
        {
            MCTSNode selectedNode = Select(root);
            MCTSNode expandedNode = Expand(selectedNode);
            float reward = Simulate(expandedNode);
            Backpropagate(expandedNode, reward);
        }
        MCTSNode bestChild = root.Children?.OrderByDescending(child => child.Visits)?.FirstOrDefault();
       //Debug.Log(bestChild.State);
        return NodeState.Success;
    }

    private MCTSNode Select(MCTSNode node)
    {
        while (node.Children.Count > 0)
        {
            node = node.SelectChild();
        }
        return node;
    }

    private MCTSNode Expand(MCTSNode node)
    {
        MCTSNode newNode = node.Expand();
        if (newNode != null)
        {
            node.Children.Add(newNode);
            return newNode;
        }
        return node; 
    }

    private float Simulate(MCTSNode node)
    {
        return Random.Range(0.0f, 1.0f);
    }

    private void Backpropagate(MCTSNode node, float reward)
    {
        while (node != null)
        {
            node.Visits++;
            node.TotalReward += reward;
            node = node.Parent;
        }
    }
}
