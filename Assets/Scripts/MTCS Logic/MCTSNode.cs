using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Zayene.UnityAITools.BehaviourTree;
public class MCTSNode
{
    public int Visits { get; set; }
    public float TotalReward { get; set; }
    public NodeState State { get; set; }
    public MCTSNode Parent { get; set; }
    public List<MCTSNode> Children { get; set; }

    public MCTSNode(NodeState state, MCTSNode parent = null)
    {
        State = state;
        Parent = parent;
        Children = new List<MCTSNode>();
        Visits = 0;
        TotalReward = 0;
    }

    public MCTSNode SelectChild()
    {
        Debug.Log("making child here");
        return Children.OrderByDescending(child => UCB1(child)).FirstOrDefault();
    }

    public MCTSNode Expand()
    {
        
        return null;
    }

    private double UCB1(MCTSNode node)
    {
        // Constants for UCB1 calculation
        const double explorationWeight = 1.0; // Adjust as needed

        // Ensure the node has been visited at least once to avoid division by zero
        if (node.Visits == 0)
        {
            return double.MaxValue;
        }

        // Calculate UCB1 value
        double exploitationTerm = node.TotalReward / node.Visits;
        double explorationTerm = explorationWeight * Math.Sqrt(Math.Log(Visits) / node.Visits);

        return exploitationTerm + explorationTerm;
    }
}
