using System.Collections.Generic;
using UnityEngine;

    public class Unit : MonoBehaviour {
        [SerializeField] private SpriteRenderer _renderer;
        public Dictionary<Vector2, NodeBase> Tiles; 
        public void Init(Sprite sprite) {
            _renderer.sprite = sprite;
        }
    }

