using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Zayene.UnityAITools.BehaviourTree;

public class SelectTargetNode : Node
{ 
    public string targetKey;
    public string conditionKey;
    public AStar.Node.NodeBase target;

    public SelectTargetNode(string _targetKey, string conditionKey)
    {
        this.targetKey = _targetKey;
        this.conditionKey = conditionKey;   
    }
    public override NodeState Evaluate()
    {
        if((bool)tree.blackBoard.GetEntry(conditionKey) == true) {

            target = AStar.Node.GridManager.Instance.Tiles.Where(t => t.Value.Walkable).OrderBy(t => Random.value).First().Value;
            tree.blackBoard.AddOrOverwriteEntry(targetKey, target);
            tree.blackBoard.AddOrOverwriteEntry(conditionKey, false);
            
        }
        state = NodeState.Success;
        return state;
    }
}
