using AStar.Node;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using Zayene.UnityAITools.BehaviourTree;
using static UnityEngine.UI.CanvasScaler;

public class AttackPlayer : Node
{
    public string targetKey;
    public AStar.Node.NodeBase target;
    public PlayerUnit unit;
    public EnemyUnit enemy;
    public AttackPlayer(string _targetKey, PlayerUnit _unit, EnemyUnit _enemy)
    {
        this.targetKey = _targetKey;
        this.unit = _unit;
        this.enemy = _enemy;
    }
    public override NodeState Evaluate()
    {
        target = AStar.Node.GridManager.Instance._playerUnit.nodeBase;

        if (enemy.GeneratePath(target).Count < 5)
        {
            enemy.isMoving = false;
            enemy.mypath.Clear();
            enemy.target = unit.transform;
            enemy.Attack();
            tree.blackBoard.AddOrOverwriteEntry(targetKey, target);
            state = NodeState.Success;
            return state;
        }


        state = NodeState.Failure;
        return state;
    }

 
}
