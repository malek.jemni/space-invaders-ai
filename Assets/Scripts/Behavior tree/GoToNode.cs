using AStar.Node;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting.Antlr3.Runtime.Tree;
using UnityEngine;
using Zayene.UnityAITools.BehaviourTree;

public class GoToNode : Node
{
    public string targetKey;
    public string conditionKey;
    public AStar.Node.NodeBase target;
    public EnemyUnit unit;

    public GoToNode(string _targetKey, EnemyUnit _unit,string _conditionKey)
    {
        this.targetKey = _targetKey;
        this.unit = _unit;
        this.conditionKey = _conditionKey;  
    }



    public override NodeState Evaluate()
    {
        target = (AStar.Node.NodeBase)tree.blackBoard.GetEntry(targetKey);


        if (target == null) {
            state = NodeState.Failure;
            return state;
        }
        else
        {
            List<AStar.Node.NodeBase> path;
            path = unit.GeneratePath(target);
            if (path.Count == 0) {
                state = NodeState.Failure;
                return state;
            }
                       
            unit.MoveToTarget(path);
            if (unit.mypath.Count == 1)
            {
                tree.blackBoard.AddOrOverwriteEntry(conditionKey, true);
                state = NodeState.Success;
                return state;
            }
            state = NodeState.Failure;
            return state;
        }
    }

}
