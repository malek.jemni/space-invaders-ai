using AStar.Node;
using Zayene.UnityAITools.BehaviourTree;

public class SelectPlayerNode : Node
{
    public string targetKey;
    public AStar.Node.NodeBase target;
    public EnemyUnit unit;

    public SelectPlayerNode(string _targetKey,EnemyUnit _unit)
    {
        this.targetKey = _targetKey;
        this.unit = _unit;
    }
    public override NodeState Evaluate()
    {
        target = AStar.Node.GridManager.Instance._playerUnit.nodeBase;
 
        if (unit.GeneratePath(target).Count < 10)
        {
            tree.blackBoard.AddOrOverwriteEntry(targetKey,target);
            state = NodeState.Success;
            return state;
        }
         
         

        state = NodeState.Failure;
        return state;
    }
}
