using AStar.Node;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zayene.UnityAITools.BehaviourTree;
public class InvaderBehaviourTree : BehaviourTree
{
    private MonteCarloTreeSearch mcts;
    public int mctsIterations = 1000;

    public override void InitTree()
    {
        blackBoard = new BlackBoard();
        blackBoard.AddOrOverwriteEntry("hasTarget", true);
        root = new Selector(this);
  
        AttackPlayer Attack = new AttackPlayer("target", AStar.Node.GridManager.Instance._playerUnit, GetComponent<EnemyUnit>());

        SelectTargetNode getTarget = new SelectTargetNode("target", "hasTarget");

        Sequence Moving = new Sequence();
        Selector targetSelector = new Selector();

        GoToNode gtn = new GoToNode("target", GetComponent<EnemyUnit>(), "hasTarget");

        SelectPlayerNode playerTarget = new SelectPlayerNode("target", GetComponent<EnemyUnit>());

        
        root.AddChild(Moving);
       

        Moving.AddChild(targetSelector);
        targetSelector.AddChild(playerTarget);
        targetSelector.AddChild(getTarget);



        Moving.AddChild(gtn);

        root.AddChild(Attack);


        // MCTS setup
        var initialGameState = root.state;
        mcts = new MonteCarloTreeSearch(initialGameState);
    }

    //public override void Update()
    //{
    //    base.Update();
    //    RunMCTSAndUpdate();
    //}

    private void PerformAction(NodeState action)
    {
        

        // Implement the logic to perform the selected action
        // This might involve using A* to navigate to a specific location or any other relevant action
        if (action == NodeState.Success)
        {
            // Perform your action here
        }
    }

    private void RunMCTSAndUpdate()
    {
        // Run MCTS simulations
        NodeState bestAction = mcts.RunMCTS(mctsIterations);
        Debug.Log($"MCTS Action: {bestAction}");

        // Use the results of MCTS for decision-making
        // Perform the action based on the MCTS result
        //PerformAction(bestAction);

        //if (bestAction == NodeState.Success)
        //{
        //    Debug.Log($"MCTS Action: {bestAction}");
        //}
        //else
        //{
        //    Debug.Log("MCTS Action: No action selected");
        //}
    }
}

