using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace AStar.Node
{
    [CreateAssetMenu(fileName = "HexBased")]
    public class GridGenerator : ScriptableObject
    {
        [SerializeField, Range(1, 50)] private int _gridWidth = 30;
        [SerializeField, Range(1, 50)] private int _gridDepth = 30;
        [SerializeField] private NodeBase basePrefab;
        [SerializeField, Range(0, 6)] private int _obstacleWeight = 3;
        public Dictionary<Vector2, NodeBase> GenerateGrid()
        {
            var tiles = new Dictionary<Vector2, NodeBase>();
            var grid = new GameObject
            {
                name = "Grid"
            };
            for (var r = 0; r < _gridDepth; r++)
            {
                var rOffset = r >> 1;
                for (var q = -rOffset; q < _gridWidth - rOffset; q++)
                {
                    var tile = Instantiate(basePrefab, grid.transform);
                    tile.Init(SetObstacle(), new HexCoords(q, r));

                    tiles.Add(tile.Coords.Pos, tile);
                }
            }

            return tiles;
        }
        protected bool SetObstacle() => Random.Range(1, 20) > _obstacleWeight;
    }
}