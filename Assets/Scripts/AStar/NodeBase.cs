using System;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using Random = UnityEngine.Random;

namespace AStar.Node {
    public abstract class NodeBase : MonoBehaviour
    {
        [Header("References")]
        [SerializeField]
        private Color _obstacleColor;

        [SerializeField] private Gradient _walkableColor;
        [SerializeField] protected GameObject _obstacleRenderer;
        [SerializeField] protected GameObject _walkableRenderer;
        [SerializeField] protected GameObject _resourcesRenderer;

        [SerializeField] public GameObject[] nodeRenderer;


        [SerializeField] private int _resourcesChance = 3;
        public ICoords Coords;
        public float GetDistance(NodeBase other) => Coords.GetDistance(other.Coords);
        public bool Walkable { get; private set; }
        private bool _selected;
        public List<Color> _defaultColors;

        public virtual void Init(bool walkable, ICoords coords)
        {
            Walkable = walkable;


            if (walkable)
            {
                if (SetResourceNode())
                    nodeRenderer[2].SetActive(true);
                else
                    nodeRenderer[0].SetActive(true);

            }
            else
                nodeRenderer[1].SetActive(true);


            for (int i = 0; i < nodeRenderer.Length; i++)
            {
                _defaultColors.Add(nodeRenderer[i].transform.GetChild(0).GetComponent<MeshRenderer>().material.color);
            }
          
            OnHoverTile += OnOnHoverTile;
            Coords = coords;
            transform.position = Coords.Pos;
        }

        public bool CheckNodeResource()
        {          
               if (nodeRenderer[2].activeInHierarchy)
                {
                    return true;
                }
            return false;
        }
        public void ResetResourceNode()
        {
            nodeRenderer[2].SetActive(false);
            nodeRenderer[0].SetActive(true);
        }

        bool SetResourceNode() => Random.Range(1, 100) < _resourcesChance;

        public static event Action<NodeBase> OnHoverTile;
        private void OnEnable() => OnHoverTile += OnOnHoverTile;
        private void OnDisable() => OnHoverTile -= OnOnHoverTile;
        private void OnOnHoverTile(NodeBase selected) => _selected = selected == this;

        protected virtual void OnMouseDown()
        {
            if (!Walkable) return;
            OnHoverTile?.Invoke(this);
        }

        #region Pathfinding

        [Header("Pathfinding")]
        [SerializeField]
        private TextMeshPro _fCostText;

        [SerializeField] private TextMeshPro _gCostText, _hCostText;
        public List<NodeBase> Neighbors { get; protected set; }
        public NodeBase Connection { get; private set; }
        public float G { get; private set; }
        public float H { get; private set; }
        public float F => G + H;

        public abstract void CacheNeighbors();

        public void SetConnection(NodeBase nodeBase)
        {
            Connection = nodeBase;
        }

        public void SetG(float g)
        {
            G = g;
            SetText();
        }

        public void SetH(float h)
        {
            H = h;
            SetText();
        }

        private void SetText()
        {
            if (_selected) return;
            _gCostText.text = G.ToString();
            _hCostText.text = H.ToString();
            _fCostText.text = F.ToString();
        }

        public void SetColor(Color color) {
            for(int i = 0; i < nodeRenderer.Length; i++)
            {
                nodeRenderer[i].transform.GetChild(0).GetComponent<MeshRenderer>().material.color = color;

            }
            }

        public void RevertTile()
        {
            for(int i = 0;i < nodeRenderer.Length;i++)
            {
                nodeRenderer[i].transform.GetChild(0).GetComponent<MeshRenderer>().material.color = _defaultColors[i];
            }        
            _gCostText.text = "";
            _hCostText.text = "";
            _fCostText.text = "";
        }
        public NodeBase getNode(int x, int y)
        {

            return this;
        }
        #endregion
    }



    public interface ICoords
    {
        public float GetDistance(ICoords other);
        public Vector2 Pos { get; set; }
    }
}

