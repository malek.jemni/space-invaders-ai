using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

namespace AStar.Node
{
    public class PlayerUnit : MonoBehaviour
    {
        [SerializeField] private SpriteRenderer _renderer;
        public Dictionary<Vector2, NodeBase> Tiles;
        public NodeBase nodeBase;
        private LineRenderer lineRenderer;
        public Transform target;
        public void Init()
        {
          
        }
        public void GetNode(NodeBase _myNodeBase)
        {
            nodeBase = _myNodeBase;
        }
        private void Update()
        {
            CollectResource();

            if(Input.GetKey(KeyCode.Space))
            {
                ShotTarget();
            }
           
        }


        public void CollectResource()
        {
            if(nodeBase.CheckNodeResource())
            {
                nodeBase.ResetResourceNode();
            }
            
        }

        public void ShotTarget()
        {

            foreach(EnemyUnit enemy in GridManager.Instance.enemyUnits) {
                List<NodeBase> way = enemy.GeneratePath(nodeBase);
                if(way.Count < 8)
                {
                    if(enemy != null)
                    {
                        target = enemy.transform;
                    }
                      
                }
            }
            
            if(target != null)
            {
                StartCoroutine(LazerLauncher());
                target.GetComponent<HealthSystem>().TakeDamage(1);
            }           
        }

        IEnumerator LazerLauncher()
        {
            lineRenderer = GetComponent<LineRenderer>();
            lineRenderer.enabled = true;
            lineRenderer.SetPosition(0,transform.position);
            lineRenderer.SetPosition(1, target.position);
           
            yield return new WaitForSeconds(0.1f);
            
            lineRenderer.enabled = false;
        }
    }
}