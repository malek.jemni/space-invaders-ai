using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace AStar.Node
{
    public class Pointer : MonoBehaviour
    {
        [SerializeField] private SpriteRenderer _renderer;
        public Dictionary<Vector2, NodeBase> Tiles;
        public void Init(Sprite sprite)
        {
            _renderer.sprite = sprite;
        }
    }
}
