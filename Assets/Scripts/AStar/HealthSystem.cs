using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthSystem : MonoBehaviour
{
    public float maxHealth = 100;  // Maximum health of the object
    public float currentHealth;     // Current health of the object

    void Start()
    {
        // Initialize current health to max health at the start
        currentHealth = maxHealth;
    }

    // Function to handle taking damage
    public void TakeDamage(float damage)
    {
        // Subtract the damage from the current health
        currentHealth -= damage;

        // Check if the health is less than or equal to 0
        if (currentHealth <= 0)
        {
            Die();  // Call a function to handle death
        }
    }
    void Die()
    {
        foreach (Behaviour component in GetComponents<Behaviour>())
        {
            component.enabled = false;
        }

        Destroy(gameObject);
    }
}

