using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Unity.VisualScripting;
using UnityEngine;
namespace AStar.Node
{
    public class GridManager : MonoBehaviour
    {
        public static GridManager Instance;

        [SerializeField] private PlayerUnit _playerPrefab;
        [SerializeField] private EnemyUnit _enemyUnitPrefab;
        [SerializeField] private GridGenerator _gridGenerator;
        [SerializeField] private bool _drawConnections;

        public List<NodeBase> mypath;
        public bool isMoving = false;
        public bool stepForward = false;

        public Dictionary<Vector2, NodeBase> Tiles { get; private set; }

        private static readonly Color OpenColor = new Color(.4f, .6f, .4f);
        public NodeBase _enemyNodeBase, _goalNodeBase, _playerNodeBase;
        public PlayerUnit _playerUnit;


        public int _spawnCount = 10; 
        public List<EnemyUnit> enemyUnits = new List<EnemyUnit>();
        void Awake() => Instance = this;

        private void Start()
        {
            Tiles = _gridGenerator.GenerateGrid();

            foreach (var tile in Tiles.Values) tile.CacheNeighbors();

            SpawnUnits();
            NodeBase.OnHoverTile += OnTileHover;
        }
        public void ClearLevel()
        {

        }

        public void LoadLevel()
        {
            Tiles.Clear();
            DestroyElements();
            enemyUnits.Clear();
            
            Tiles = _gridGenerator.GenerateGrid();

            foreach (var tile in Tiles.Values) tile.CacheNeighbors();

            SpawnUnits();
            NodeBase.OnHoverTile += OnTileHover;
        }
        private void Update()
        {
            if(Tiles.Where(t => t.Value.nodeRenderer[2].activeInHierarchy).Count() == 0)
            { LoadLevel(); }
          
            if (mypath.Count > 0)
            {
                if (isMoving)
                {
                    if (!stepForward)
                        StartCoroutine(Wait());
                }
            }
            else
            {
                isMoving = false;
            }

        }

        private void OnDestroy() => NodeBase.OnHoverTile -= OnTileHover;

        private void OnTileHover(NodeBase nodeBase)
        {
            _goalNodeBase = nodeBase;
        
            
            foreach (var t in Tiles.Values) t.RevertTile();

            _goalNodeBase.SetColor(OpenColor);

            var path = Pathfinding.FindPath(_playerNodeBase, _goalNodeBase);
            Move(path);

        }

        void SpawnUnits()
        {
            for(int i = 0; i < _spawnCount; i++)
            {
                _enemyNodeBase = Tiles.Where(t => t.Value.Walkable).OrderBy(t => Random.value).First().Value;
                enemyUnits.Add(Instantiate(_enemyUnitPrefab, _enemyNodeBase.Coords.Pos, Quaternion.Euler(-90, 0, 0)));

                SetEnemySpawn(enemyUnits[i]);
            }

            _playerNodeBase = Tiles.Where(t => t.Value.Walkable).OrderBy(t => Random.value).First().Value;
            _playerUnit = Instantiate(_playerPrefab, _playerNodeBase.Coords.Pos,Quaternion.Euler(-90, 0, 0));
            _playerUnit.Init();
            _playerUnit.GetNode(_playerNodeBase);
 
        }
        private void DestroyElements()
        {
            foreach (var enemy in enemyUnits) {  Destroy(enemy.gameObject); }
            Destroy(_playerUnit.gameObject);
            Destroy(GameObject.Find("Grid"));

        }

        private void SetEnemySpawn(EnemyUnit enemy)
        {
            enemy.GetNode(_enemyNodeBase);
        }



        private void Move(List<NodeBase> path)
        {
            mypath.Clear();
            foreach (NodeBase node in path)
            {
                mypath.Add(node);
            }
            isMoving = true;
        }

        public IEnumerator Wait()
        {
            stepForward = true;       
            _playerNodeBase = mypath[mypath.Count - 1];
            _playerUnit.transform.position = _playerNodeBase.Coords.Pos;
            _playerUnit.GetNode(_playerNodeBase);
            mypath.RemoveAt(mypath.Count - 1);

            yield return new WaitForSeconds(0.2f);
            stepForward = false;
        }


    }

}