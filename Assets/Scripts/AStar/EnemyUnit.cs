using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;
using static UnityEngine.GraphicsBuffer;

namespace AStar.Node
{
    public class EnemyUnit : MonoBehaviour
    {
        public Dictionary<Vector2, NodeBase> Tiles;

        public NodeBase nodeBase;
        private LineRenderer lineRenderer;
        public List<NodeBase> mypath;
        public Transform target;
        public bool isMoving = false;
        public bool stepForward = false;

        public void Init()
        {
          
        }
        public void GetNode(NodeBase _myNodeBase)
        {
            nodeBase = _myNodeBase;
        }
        private void Update()
        {
            if (!isMoving) { 
                return; 
            }
            if (!stepForward) StartCoroutine(Wait());
          
        }

        public List<NodeBase> GeneratePath(NodeBase target) 
        {
            return Pathfinding.FindPath(nodeBase, target);
        }

        public void MoveToTarget(List<NodeBase> path)
        {
            isMoving = true;
            Move(path);
        }
        public void GetPostion()
        {
            NodeBase _goalNodeBase;
            _goalNodeBase = GridManager.Instance.Tiles.Where(t => t.Value.Walkable).OrderBy(t => Random.value).First().Value;

            foreach (var t in GridManager.Instance.Tiles.Values) t.RevertTile();

            var path = Pathfinding.FindPath(nodeBase, _goalNodeBase);
            isMoving = true;
            Move(path);
        }

        public void Patrol()
        {

        }
        public void Attack()
        {
            if (target != null)
            {
                StartCoroutine(LazerLauncher());
                target.GetComponent<HealthSystem>().TakeDamage(0.1f);
            }

        }

        private void Move(List<NodeBase> path)
        {
            mypath.Clear();
            foreach (NodeBase node in path)
            {
                mypath.Add(node);
            }           
        }

        public IEnumerator Wait()
        {
            stepForward = true;
            if (mypath.Count > 0)
            {
                nodeBase = mypath[mypath.Count - 1];
                transform.position = nodeBase.Coords.Pos;
                mypath.RemoveAt(mypath.Count - 1);
            }
            else isMoving = false;
            yield return new WaitForSeconds(0.5f);
            stepForward = false;           
        }

        IEnumerator LazerLauncher()
        {
            lineRenderer = GetComponent<LineRenderer>();
            lineRenderer.enabled = true;
            lineRenderer.SetPosition(0, transform.position);
            lineRenderer.SetPosition(1, target.position);

            yield return new WaitForSeconds(0.1f);

            lineRenderer.enabled = false;
        }

    }

}